import Vue from 'vue'
import Vuex from 'vuex'
import {home} from '@/api/home/index.js'
import router from '../router'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // home_data : {}
  },
  mutations: {
    addSync(state,data){
      state.home_data = data
    },
    to(ph){
      console.log(this.$router)
      this.$router.push({
        path:ph
      })
    }
  },
  actions: {
    onLoad(context) {
      home(
      )
      .then(res =>{
        console.log(res)
        context.commit('addSync',res)
      })
    }
  },
  modules: {
    
  },
  getters :{
    data(){
      home(
        )
        .then(res =>{
          console.log(res)
          return res
        })
    }
  }
})
