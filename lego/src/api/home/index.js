//导入api配置文件
import request from "@/api/request"

function home(){
  return request({
    method:"get",
    url:"/index/index",
  })
}

// export default a  // import abc
export  { //  import { home } 
  home
}