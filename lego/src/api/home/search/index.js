//导入api配置文件
import request from "@/api/request"


function searchHot( data){
  return request({
    method:'get',
    url:'/search/indexaction',
    data 
  })
}

function serchHint(data) {
  return request({
    method:'get',
    url:'/search/helperaction',
    data 
  })
}

function addKeyword(data) {
  return request({
    method:'post',
    url:'/search/addhistoryaction',
    data 
  })
}

function delKeyword(data) {
  return request({
    method:'post',
    url:'/search/clearhistoryAction',
    data 
  })
}
// export default a  // import abc
export  { //  import { home } 
  searchHot ,serchHint,addKeyword,delKeyword
}