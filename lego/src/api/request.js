import axios from "axios";
import qs from "qs";

import Vue from 'vue';
import { Toast } from 'vant';

Vue.use(Toast);
var host;
// process.env是Node.js提供的一个api,他返回一个包含用户环境的信息对象
//通过NODE_ENV 判断当前环境是生产环境(production)还是开发环境(development) 自动切换不同的域名
//暂时开发环境和生产环境 所以域名一样
if(process.env.NODE_ENV == 'development') {
  host = "http://shop.bufantec.com/bufan"
}else {
  host = "http://shop.bufantec.com/bufan";
}

//创建axios实例
const instance = axios.create({
  // baseURL 公共接口路径
  baseURL: host,
  //timeout 超时时间
  // timeout:5000
});

//设置请求拦截器
instance.interceptors.request.use(
  config => {
    if (config.method.toLowerCase() == "post") {
      // qs序列化
      // https://www.npmjs.com/package/qs
      //  arrayFormat: 'repeat' 作用:  { a: ['b', 'c'] }  ==> 'a=b&a=c'
      //  allowDots: true  作用: { a: { b: { c: 'd', e: 'f' } } }  ==>  'a.b.c=d&a.b.e=f'
      config.data = qs.stringify(config.data, { arrayFormat: 'repeat', allowDots: true })
    } else {
      config.params = config.data
    }
    // setTimeout(function(){
      Toast.loading({
        message: '加载中...',
        forbidClick: true,
        loadingType: 'spinner',
      });
    // },1000)
    return config
  },
  error => {
    // do something with request error
    Toast.fail('请求失败');
    console.log(error) //for debug
    return Promise.reject(error)
  }
)

// 响应式拦截

instance.interceptors.response.use(

  response =>{
    const res = response.data
    Toast.clear()
    return res
  },
  error => {
    Toast.fail('请求失败');
    console.log('err' + error) //for debug
    return Promise.reject(error)
  }
)

export default instance