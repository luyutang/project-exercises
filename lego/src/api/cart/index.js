import request from "@/api/request"

function getCart(data){
  return request({
    method:"get",
    url:"/cart/cartList",
    data
  })
}

function addCart(data){
  return request({
    method:"post",
    url:"/cart/addCart",
    data
  })
}

function delCart(data){
  return request({
    method:"get",
    url:"/cart/deleteAction",
    data
  })
}

// export default a  // import abc
export  { //  import { home } 
getCart,addCart,delCart
}