//导入api配置文件
import request from "@/api/request"

function getAdd(data){
  return request({
    method:"get",
    url:"/order/detailAction",
    data
  })
}

function subOrder(data){
  return request({
    method:"post",
    url:"/order/submitAction",
    data
  })
}

// export default a  // import abc
export  { //  import { home } 
getAdd,subOrder
}