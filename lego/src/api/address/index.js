//导入api配置文件
import request from "@/api/request"

function delAdd(data){
  return request({
    method:"get",
    url:"/address/deleteAction",
    data
  })
}

function getAddinfo(data){
  return request({
    method:"get",
    url:"/address/detailAction",
    data
  })
}

function getAddlist(data){
  return request({
    method:"get",
    url:"/address/getListAction",
    data
  })
}

function saveAdd(data){
  return request({
    method:"post",
    url:"/address/saveAction",
    data
  })
}

// export default a  // import abc
export  { //  import { home } 
delAdd,getAddinfo,getAddlist,saveAdd
}