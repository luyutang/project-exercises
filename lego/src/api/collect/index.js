import request from "@/api/request"

function getCollect(data){
  return request({
    method:"get",
    url:"/collect/listAction",
    data
  })
}

function addCollect(data){
  return request({
    method:"post",
    url:"/collect/addcollect",
    data
  })
}

// export default a  // import abc
export  { //  import { home } 
getCollect,addCollect
}