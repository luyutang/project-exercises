//导入api配置文件
import request from "@/api/request"

function topic(data){
  return request({
    method:"get",
    url:"/topic/listaction",
    data
  })
}

function info(data){
  return request({
    method:"get",
    url:"/topic/detailaction",
    data
  })
}

// export default a  // import abc
export  { //  import { home } 
topic,info
}