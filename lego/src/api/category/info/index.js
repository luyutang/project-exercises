import request from "@/api/request"

function getNav(data){
  return request({
    method:"get",
    url:"/category/categoryNav",
    data
  })
}

function getGoods(data){
  return request({
    method:"get",
    url:"/goods/goodsList",
    data
  })
}

// export default a  // import abc
export  { //  import { home } 
getNav,getGoods
}