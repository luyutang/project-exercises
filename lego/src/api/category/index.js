import request from "@/api/request"

function getNav(data){
  return request({
    method:"get",
    url:"/category/indexaction",
    data
  })
}

function getNav_selsct(data){
  return request({
    method:"get",
    url:"/category/currentaction",
    data
  })
}

// export default a  // import abc
export  { //  import { home } 
getNav,getNav_selsct
}