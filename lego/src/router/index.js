import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  //默认路径
  {
    path:"/",
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('../views/home/index.vue'),
    //nate 路由元信息 自定义属性
    meta : {
      isShow:true
    }
  },
   {
    path: '/cart',
    name: 'cart',
    component: () => import('../views/cart/index.vue'),
    meta : {
      isShow:true
    }
  },
  {
    path: '/topic',
    name: 'topic',
    component: () => import('../views/topic/index.vue'),
    meta : {
      isShow:true
    }
  },
  {
    path: '/my/collect',
    name: 'my',
    component: () => import('../views/my/collect/index.vue'),
    meta : {
      isShow:false,
      goBack:true
    }
  },
  {
    path: '/my/opinion',
    name: 'my',
    component: () => import('../views/my/opinion/index.vue'),
    meta : {
      isShow:false,
      goBack:true
    }
  },
  {
    path: '/my/',
    name: 'my',
    component: () => import('../views/my/index.vue'),
    meta : {
      isShow:true
    }
  },
  {
    path: '/city',
    name: 'city',
    component: () => import('../views/home/city/index.vue'),
    meta : {
      isShow:false,
      goBack:true
    }
  },
  {
    path: '/category',
    name: 'category',
    component: () => import('../views/category/index.vue'),
    meta : {
      isShow:true
    }
  },
  {
    path: '/category/info',
    name: 'info',
    component: () => import('../views/category/info/index.vue'),
    meta : {
      goBack:true
    }
  },
  {
    path: '/search',
    name: 'search',
    component: () => import('../views/home/search/index.vue'),
    meta : {
      isShow:false,
      goBack:true
    }
  },
  {
    path: '/topic/info',
    name: 'info',
    component: () => import('../views/topic/info/index.vue'),
    meta : {
      isShow:false,
      goBack:true
    }
  },
  {
    path: '/commodity',
    name: 'commodity',
    component: () => import('../views/commodity/index.vue'),
    meta : {
      isShow:false,
      goBack:true
    }
  },
  {
    path: '/order',
    name: 'order',
    component: () => import('../views/order/index.vue'),
    meta : {
      isShow:false,
      goBack:true
    }
  },
  {
    path: '/order/address',
    name: 'address',
    component: () => import('../views/order/address/index.vue'),
    meta : {
      isShow:false,
      goBack:true
    }
  },
  {
    path: '/order/add',
    name: 'add',
    component: () => import('../views/order/add/index.vue'),
    meta : {
      isShow:false,
      goBack:true
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
