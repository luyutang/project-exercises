import baseUrl from "./baseUrl";

// 封装之后的使用与axios封装之后的使用保持一致
//axios.get("/abc",{})
var obj = {
  get(url,data){
    // Promise  容器  里面保存了异步操作的结果
    return new Promise((resolve,rejected)=>{
      wx.request({
        url: baseUrl + url,
        data,
        success(res){
          resolve(res);
        },
        fail(res){
          resolve(res);
        }
      })
    })
  },
  post(url,data){
    return new Promise((resolve,rejected)=>{
      wx.request({
        url: baseUrl + url,
        data,
        method:"POST",
        header:{
          'content-type':"application/x-www-form-urlencoded"
        },
        success(res){
          resolve(res);
        },
        fail(res){
          resolve(res);
        }
      })
    })
  }
}

// function home(data){
//   return http({
//     url:"",
//     method:"",
//     data
//   })
// }

function http(config){
  if(config.method.toLowerCase() == "post"){
    return obj.post(config.url,config.data)
  }else{
    return obj.get(config.url,config.data)
  }
}

export default obj;

export {
  http
}
