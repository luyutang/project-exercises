import { http } from "../http"

//即将热映
function coming_soon(data){
  return http({
    url:"/api/douban/movie/coming_soon",
    method:"GET",
    data
  })
}

//正在热映
function in_theaters(data){
  return http({
    url:"/api/douban/movie/in_theaters",
    method:"GET",
    data
  })
}
//排行250
function top250(data){
  return http({
    url:"/api/douban/movie/top250",
    method:"GET",
    data
  })
}
//精彩预告
function selectPreview(data){
  return http({
    url:"/api/douban/movie/selectPreview",
    method:"GET",
    data
  })
}

export {
  coming_soon,
  in_theaters,
  top250,
  selectPreview
}
