//index.js
//获取应用实例
const app = getApp()
import {  
  coming_soon,
  in_theaters,
  top250,
  selectPreview} from "../../utils/home/home.js"

Page({
  data: {
    url:['/assets/home/banner1.jpg','/assets/home/banner2.jpg','/assets/home/banner3.jpg'],
    swiperIndex:0,
    coming_soon_list:[],
    in_theaters_list:[],
    selectPreview_list:[]
  },
  //事件处理函数
  swiperChange(e) {
    const that = this;
    that.setData({
      swiperIndex: e.detail.current,
    })
  },
  init:function(){
    let that = this;
    coming_soon({

    }).then(res =>{
      console.log(res);
      var list=res.data.data.list
      for(var i=0;i<list.length;i++){
        var data = list[i];
        console.log(data)
        data.day = data.show_date[0].substring(5,10).split('-').join('月')+'日'
      }
       that.setData({
         in_theaters_list:list
       })
    })
  },
  init1:function(){
    let that = this;
    in_theaters()
    .then(res =>{
      console.log(res);
      that.setData({
        coming_soon_list:res.data.data.list
      })
    })
  },
  init2:function(){
    let that = this;
    selectPreview()
    .then(res => {
      console.log(res)
      that.setData({
        selectPreview_list:res.data.data
      })
    })
  },
    /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.init();
    this.init1();
    this.init2();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
